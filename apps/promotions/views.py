from django.core.urlresolvers import reverse
from django.views.generic import RedirectView, TemplateView

class HomeView(TemplateView):
    template_name = "promotions/home.html"
