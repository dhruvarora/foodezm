from oscar.apps.catalogue.models import Category


def categories(request):
    context = dict()
    context["categories"] = Category.objects.all()

    return context
