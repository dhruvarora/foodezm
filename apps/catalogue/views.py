import warnings

from django.contrib import messages
from django.core.paginator import InvalidPage
from django.http import Http404, HttpResponsePermanentRedirect
from django.shortcuts import get_object_or_404, redirect
from django.utils.http import urlquote
from django.utils.translation import ugettext_lazy as _
from django.views.generic import DetailView, TemplateView, View
from django.shortcuts import render

from oscar.apps.catalogue.signals import product_viewed
from oscar.core.loading import get_class, get_model

Product = get_model('catalogue', 'product')
Category = get_model('catalogue', 'category')
ProductAlert = get_model('customer', 'ProductAlert')
ProductAlertForm = get_class('customer.forms', 'ProductAlertForm')
get_product_search_handler_class = get_class(
    'catalogue.search_handlers', 'get_product_search_handler_class')


class ProductCategoryView(View):
    template_name = "catalogue/category.html"

    def get_category(self):
        if "pk" in self.kwargs:
            return get_object_or_404(Category, pk=self.kwargs['pk'])

    def get(self, request, pk, category_slug):
        category = self.get_category()
        products = Product.objects.filter(categories=category.pk)

        return render(request, self.template_name, {
        "category":category,
        "products":products,
        })
